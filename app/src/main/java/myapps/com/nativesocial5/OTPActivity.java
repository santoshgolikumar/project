package myapps.com.nativesocial5;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OTPActivity extends AppCompatActivity {
    EditText mobile,otp;
    Button submit,getotp;
    private String TAG2 = "wb";
    private BroadcastReceiver receiver;
    public  static final String EM="eid";

    public static final String USER_OTP_URL="http://35.167.108.80:8080/HappServices/happ/test/otpstore";
    public static final String USER_OTPVERIFY_URL="http://35.167.108.80:8080/HappServices/happ/test/otpverify";
    SmsVerifyCatcher smsVerifyCatcher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        mobile=(EditText)findViewById(R.id.emailenter);
        otp=(EditText)findViewById(R.id.otp);
        getotp=(Button)findViewById(R.id.sendemail);
        submit=(Button)findViewById(R.id.verifyemail);


        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = parseCode(message);//Parse verification code
                //etCode.setText(code);//set code in edit text
                Toast.makeText(OTPActivity.this,code,Toast.LENGTH_LONG).show();
                otp.setText(code);
                //then you can send verification code to server
            }
        });


        final HashMap<String, String> postOTPDataParams,postOTPParams;
        postOTPDataParams = new HashMap<String, String>();
        postOTPParams = new HashMap<String, String>();
        getotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getotp.setText("Resend OTP");


                Intent y=getIntent();
                String emp_id=y.getStringExtra(RegisterActivity.OTPKEY);

                String m=mobile.getText().toString();
                if(m.length()==10){
                postOTPDataParams.put("uid",emp_id);
                postOTPDataParams.put("mobile_number",m);
                invokeOTPWS(postOTPDataParams);}
                else{
                    Toast.makeText(OTPActivity.this,"Enter a valid mobile number",Toast.LENGTH_LONG).show();
                }

            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //postOTPParams.put("uotp",mobile.getText().toString());
                Intent y=getIntent();
                String emp_id=y.getStringExtra(RegisterActivity.OTPKEY);
                String o=otp.getText().toString();
                postOTPParams.put("uid",emp_id);
                postOTPParams.put("uotp",o);

                invokeOTPVerifyWS(postOTPParams);
                getPermissionToReadContacts();


            }
        });
    }


    public void invokeOTPWS(HashMap<String, String> params) {
        // Show Progress Dialog
        //prgDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                USER_OTP_URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //prgDialog.hide();
                        try {
                            Log.i(TAG2, "Sign Up Response :: " + response);
                            Toast.makeText(OTPActivity.this,response.getString("status"),Toast.LENGTH_LONG).show();
                            if(response.getString("status").equals("success")){


                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }



                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG2, "Error: " + error.getMessage());
                //prgDialog.hide();
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //params.put("Authorization", Const.CAD_ACCESS_TOKEN);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG2);

    }


    public void invokeOTPVerifyWS(HashMap<String, String> params) {
        // Show Progress Dialog
        //prgDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                USER_OTPVERIFY_URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //prgDialog.hide();
                        try {
                            Log.i(TAG2, "Sign Up Response :: " + response);
                            Toast.makeText(OTPActivity.this,response.getString("status"),Toast.LENGTH_LONG).show();
                            if(response.getString("status").equalsIgnoreCase("success")){
                                Intent x=new Intent(OTPActivity.this,EmailActivity.class);
                                startActivity(x);

                            }


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }



                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG2, "Error: " + error.getMessage());
                //prgDialog.hide();
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //params.put("Authorization", Const.CAD_ACCESS_TOKEN);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG2);

    }

    private String parseCode(String message) {
        //Pattern p = Pattern.compile("\\b\\d{4}\\b");
        //Matcher m = p.matcher(message);
        String code = "";
       // while (m.find()) {
           // code = m.group(0);

        //}


        code= message.substring(15, message.length());
        return  code;



        //return code;
    }



    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
        //checking wether the permission is already granted





    }
    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }
    private static final int READ_SMS_PERMISSIONS_REQUEST = 1;
    public void getPermissionToReadContacts() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)!= PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{ Manifest.permission.READ_CONTACTS},READ_SMS_PERMISSIONS_REQUEST);
            }
        }
    }
    /**
     * need for Android 6 real time permissions
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


          smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }












}
