package myapps.com.nativesocial5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EmailActivity extends AppCompatActivity {
    EditText email;Button send,verify;
    public static final String USER_EMAIL_URL="http://35.167.108.80:8080/HappServices/happ/test/mailstore";
    public static final String USER_EMAILVERIFY_URL="http://35.167.108.80:8080/HappServices/happ/test/checkfor/home";
    public static final String TAG2="";
    SessionManager session;
    public static final String nativesignupuid="nativesignup";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        session = new SessionManager(getApplicationContext());

        email=(EditText)findViewById(R.id.emailenter);
       email.setText(RegisterActivity.EMAILKEY);
        //Toast.makeText(EmailActivity.this,RegisterActivity.EMAILKEY,Toast.LENGTH_LONG).show();

        send=(Button)findViewById(R.id.sendemail);
        verify=(Button)findViewById(R.id.verify);
        final HashMap<String, String> postEmailDataParams,postEmailVerifyParams;
        postEmailDataParams = new HashMap<String, String>();
        postEmailVerifyParams = new HashMap<String, String>();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent em=getIntent();
               String emp_id= RegisterActivity.O;
               String id=email.getText().toString();
                postEmailDataParams.put("uid",emp_id);
                postEmailDataParams.put("email_id",id);
                invokeEmailWS(postEmailDataParams);


            }
        });
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent em=getIntent();
                String emp_id= RegisterActivity.O;
                Toast.makeText(EmailActivity.this,emp_id,Toast.LENGTH_LONG).show();
                String emailid=email.getText().toString();


                //String hashkey= String.valueOf(email.getText().toString().hashCode());
                postEmailVerifyParams.put("email",RegisterActivity.EMAILKEY);
                postEmailVerifyParams.put("id",emp_id);

                //postEmailDataParams.put("hashkey",hashkey);
                invokeEmailVerifyWS(postEmailVerifyParams);


            }
        });




    }
    public void invokeEmailWS(HashMap<String, String> params) {
        // Show Progress Dialog
        //prgDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                USER_EMAIL_URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //prgDialog.hide();
                        try {
                            Log.i(TAG2, "Sign Up Response :: " + response);
                            Toast.makeText(EmailActivity.this,response.getString("status"),Toast.LENGTH_LONG).show();

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }



                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG2, "Error: " + error.getMessage());
                //prgDialog.hide();
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //params.put("Authorization", Const.CAD_ACCESS_TOKEN);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG2);

    }


    public void invokeEmailVerifyWS(HashMap<String, String> params) {
        // Show Progress Dialog
        //prgDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                USER_EMAILVERIFY_URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //prgDialog.hide();
                        try {
                            Log.i(TAG2, "Sign Up Response :: " + response);
                            Toast.makeText(EmailActivity.this,response.getString("status"),Toast.LENGTH_LONG).show();
                            if(response.getString("status").equalsIgnoreCase("success")){
                                session.createLoginSession(RegisterActivity.O,RegisterActivity.EMAILKEY);
                                Intent natsignup=new Intent(EmailActivity.this,HomeActivity.class);
                               // natsignup.putExtra(nativesignupuid,RegisterActivity.O);
                                //natsignup.putExtra()
                                startActivity(natsignup);


                            }



                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }



                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG2, "Error: " + error.getMessage());
                //prgDialog.hide();
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //params.put("Authorization", Const.CAD_ACCESS_TOKEN);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG2);

    }







}
