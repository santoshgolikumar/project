package myapps.com.nativesocial5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import org.json.JSONException;
import org.json.JSONObject;

public class FacebookActivity extends AppCompatActivity {
    Button logout;
    TextView name,email;
    ImageView dp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_facebook);
        name=(TextView)findViewById(R.id.Name);
        email=(TextView)findViewById(R.id.emailenter);
        logout=(Button)findViewById(R.id.logout);
        try {
            JSONObject obj = new JSONObject(getIntent().getStringExtra(MainActivity.JKEY));
            name.setText(obj.getString("name"));
            email.setText(obj.getString("email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logOut();
                Intent i=new Intent(FacebookActivity.this,MainActivity.class);
                startActivity(i);
                finish();

                return;

            }
        });

    }

}

