package myapps.com.nativesocial5;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{

    ////merge
    // Progress Dialog Object
    ProgressDialog prgDialog;
    // Error Msg TextView Object
    TextView errorMsg;
    // Email Edit View Object
    EditText emailET;
    // Passwprd Edit View Object
    EditText pwdET;
    private String TAG1 = "wb";
    private String TAG2 = "wbg";
    LinearLayout l1,l2;
    SessionManager session;

    public static final String USER_LOGIN_URL="http://35.167.108.80:8080/HappServices/happ/test/login";
    public static final String USER_SOCIAL_URL="http://35.167.108.80:8080/HappServices/happ/test/gplusfb/signup";
    public static final  String loginid="lid";




    //merge
    public  static final String GDNAME="dname";
    public  static final String GID="gid";


    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;

    public static final String  nativeuid="nativeuid";
    public static final String  nativemail="nativeemail";










    LoginButton fb;
    SignInButton gplus;
    TextView clickhere;

    CallbackManager callbackManager = CallbackManager.Factory.create();
    public  static final String JKEY="key";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main_merge);
        /////sessions
        session = new SessionManager(getApplicationContext());
        if(session.isLoggedIn()==true){
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));

        }










        ///merge
        errorMsg = (TextView) findViewById(R.id.login_error);
        // Find Email Edit View control by ID
        emailET = (EditText) findViewById(R.id.loginEmail);
        // Find Password Edit View control by ID
        pwdET = (EditText) findViewById(R.id.loginPassword);
        // l1=(LinearLayout)findViewById(R.id.layout);
        //l1.setVisibility(View.VISIBLE);
        //l2=(LinearLayout)findViewById(R.id.layout);
        //emailET.setVisibility(View.INVISIBLE);

        //l2.setVisibility(View.INVISIBLE
        clickhere=(TextView)findViewById(R.id.clickhere);
        SpannableString content = new SpannableString("New User?Create an account");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        clickhere.setText(content);
        clickhere.setVisibility(View.INVISIBLE);





        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        //merge


        fb = (LoginButton) findViewById(R.id.fb);


//fb implementation
        fb.setReadPermissions(Arrays.asList("public_profile, email, user_birthday"));

        fb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                fb.setVisibility(View.INVISIBLE);


                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {


                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                String userProviderType = "fb";
                                try {
                                    String userName = object.getString("name");
                                    String userProviderKey = object.getString("id");
                                    String emailId = object.getString("email");
                                    String password = object.getString("id");
                                    HashMap<String, String> postFbDataParams;
                                    postFbDataParams = new HashMap<String, String>();
                                    postFbDataParams.put("userProviderType", userProviderType);
                                    postFbDataParams.put("userName", userName);
                                    postFbDataParams.put("emailId", emailId);
                                    postFbDataParams.put("userProviderKey", userProviderKey);
                                    postFbDataParams.put("password", password);
                                    invokeFbWS(postFbDataParams);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                //Log.v("Main", response.toString());
                                Intent intent = new Intent(MainActivity.this, FacebookActivity.class);
                                intent.putExtra(JKEY, object.toString());
                                startActivity(intent);
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(MainActivity.this, "error to Login Facebook", Toast.LENGTH_SHORT).show();
            }
        });//fb implementation

        //Google Implementation
        SignInButton gplus;
        gplus = (SignInButton) findViewById(R.id.sign_in_button);
        gplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gintent = new Intent(MainActivity.this, GoogleActivity.class);
                startActivity(gintent);


            }
        });
         //gplus.setOnClickListener(this);

    }









    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if(requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        //mGoogleApiClient.connect();
        else{
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
    /*
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }*/
    public void loginUser(View view){
        // Get Email Edit View Value
        String email = emailET.getText().toString();
        // Get Password Edit View Value
        String password = pwdET.getText().toString();
        // Instantiate Http Request Param Object
        //RequestParams params = new RequestParams();
        HashMap<String, String> postDataParams;
        postDataParams = new HashMap<String, String>();

        // When Email Edit View and Password Edit View have values other than Null
        if(Utility.isNotNull(email) && Utility.isNotNull(password)){
            // When Email entered is Valid

            if(Utility.validate(email)){
                // Put Http parameter username with value of Email Edit View control
                postDataParams.put("email_id", email);
                // Put Http parameter password with value of Password Edit Value control
                postDataParams.put("password", password);
                // Invoke RESTful Web Service with Http parameters
                invokeWS(postDataParams);
            }
            // When Email is invalid
            else{
                Toast.makeText(getApplicationContext(), "Please enter valid email", Toast.LENGTH_LONG).show();
            }
        } else{
            Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
        }

    }
    public void invokeWS(HashMap<String, String> params) {
        // Show Progress Dialog
        prgDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                USER_LOGIN_URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        prgDialog.hide();
                        try {
                            Log.i(TAG1, "Sign Up Response :: " + response);
                            Toast.makeText(MainActivity.this,response.getString("result"),Toast.LENGTH_LONG).show();
                            if(response.getString("result").equalsIgnoreCase("mobile and email verified")){
                                session.createLoginSession(response.getString("id"),response.getString("email"));
                                Intent nat=new Intent(MainActivity.this,HomeActivity.class);
                                //loginid=response.getString("id");
                                nat.putExtra(nativeuid,response.getString("id"));
                                nat.putExtra(nativemail,response.getString("email"));
                                startActivity(nat);


                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }



                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG1, "Error: " + error.getMessage());
                prgDialog.hide();
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //params.put("Authorization", Const.CAD_ACCESS_TOKEN);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG1);

    }



  /*  public void navigatetoHomeActivity(){
        Intent homeIntent = new Intent(getApplicationContext(),HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }*/

    /**
     * Method gets triggered when Register button is clicked
     *
     * @param view
     */


    public void navigatetoRegisterActivity(View view){
        Intent loginIntent = new Intent(getApplicationContext(),RegisterActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
    }


    public void invokeFbWS(HashMap<String, String> params) {
        // Show Progress Dialog
        prgDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                USER_SOCIAL_URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        prgDialog.hide();
                        try {
                            Log.i(TAG1, "Sign Up Response :: " + response);
                            Toast.makeText(MainActivity.this,response.getString("status"),Toast.LENGTH_LONG).show();

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }



                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG1, "Error: " + error.getMessage());
                prgDialog.hide();
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //params.put("Authorization", Const.CAD_ACCESS_TOKEN);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG1);

    }

    @Override
    public void onClick(View view) {




        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(AppIndex.API).build();
        mGoogleApiClient.connect();

        signIn();

    }



    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG1, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {

            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            String userProviderType="gmail";
            String userName=acct.getDisplayName();
            String userProviderKey=acct.getId();
            String emailId=acct.getEmail();
            String password=acct.getId();
            // Log.i("name",userName);
            // Log.i("id",userProviderKey);
            // Log.i("email",emailId);
            HashMap<String, String> postGDataParams;
            postGDataParams = new HashMap<String, String>();
            postGDataParams.put("userProviderType",userProviderType);
            postGDataParams.put("userName",userName);
            postGDataParams.put("emailId",emailId);
            postGDataParams.put("userProviderKey",userProviderKey);
            postGDataParams.put("password",password);
            invokeGWS(postGDataParams);


           // dname.setText(acct.getDisplayName());
           // email.setText(acct.getEmail());


            //mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            //updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            //updateUI(false);
        }
    }

    public void signIn() {

        mGoogleApiClient.connect();



        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signIn]

    // [START signOut]
    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        //updateUI(false);

                        // [END_EXCLUDE]
                    }
                });
        //Intent y=new Intent(GoogleActivity.this,MainActivity.class);
        //Intent h=new Intent(GoogleActivity.this,MainActivity.class);
        //dname.setVisibility(View.INVISIBLE);
        //email.setVisibility(View.INVISIBLE);
        //startActivity(y);
        //finish();
    }
    // [END signOut]

    // [START revokeAccess]
    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        //updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END revokeAccess]

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG1, "onConnectionFailed:" + connectionResult);
    }



    public void invokeGWS(HashMap<String, String> params) {
        // Show Progress Dialog
        //prgDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                USER_SOCIAL_URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //prgDialog.hide();
                        try {
                            Log.i(TAG1, "Sign Up Response :: " + response);
                            Toast.makeText(MainActivity.this,response.getString("status"),Toast.LENGTH_LONG).show();
                            if(response.getString("status").equals("success")){

                                //emailET.setVisibility(View.INVISIBLE);

                                //l1.setVisibility(View.INVISIBLE);
                                //l2=(LinearLayout)findViewById(R.id.layoutt);
                                //l2.setVisibility(View.VISIBLE);







                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }



                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG1, "Error: " + error.getMessage());
                prgDialog.hide();
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //params.put("Authorization", Const.CAD_ACCESS_TOKEN);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG1);

    }
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Google Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }













    }
