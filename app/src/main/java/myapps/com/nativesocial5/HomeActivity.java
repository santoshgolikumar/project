package myapps.com.nativesocial5;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HomeActivity extends AppCompatActivity {
    FloatingActionButton edit;
    AlertDialog.Builder builder1;
    public static final String USER_EDITPROFILE_URL="http://35.167.108.80:8080/HappServices/happ/test/edit/profile";
    public static  final String TAG="";
     EditText home_name,home_dob,home_email,home_mobile;
    TextView tvname,tvdob,tvmobile,tvemail;


    // Session Manager Class
    SessionManager session;
    Button logout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Session class instance
        session = new SessionManager(getApplicationContext());

        //TextView lblName = (TextView) findViewById(R.id.name);
        //TextView lblEmail = (TextView) findViewById(R.id.email);

        // Button logout
        logout=(Button)findViewById(R.id.lout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(session.isLoggedIn())
                session.logoutUser();

            }
        });
        session.checkLogin();




        Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), Toast.LENGTH_LONG).show();

        LayoutInflater factory = LayoutInflater.from(this);

        final View textEntryView = factory.inflate(R.layout.text_entry, null);

             home_name= (EditText) textEntryView.findViewById(R.id.home_name);
            home_dob = (EditText) textEntryView.findViewById(R.id.home_dob);
             home_mobile = (EditText) textEntryView.findViewById(R.id.home_mobile);
        home_email = (EditText) textEntryView.findViewById(R.id.home_email);

        tvname = (TextView)textEntryView.findViewById(R.id.tvname);
        tvdob = (TextView)textEntryView.findViewById(R.id.tvdob);

        tvmobile = (TextView)textEntryView.findViewById(R.id.tvmobile);
        tvemail = (TextView)textEntryView.findViewById(R.id.tvemail);



        //Intent nat2=getIntent();
        //String userid=nat2.getStringExtra(nativeuid);
        //String useremail=nat2.getStringExtra(MainActivity.nativemail);
        builder1 = new AlertDialog.Builder(HomeActivity.this);



        //input1.setText(userid, TextView.BufferType.EDITABLE);
        //input2.setText(useremail, TextView.BufferType.EDITABLE);




        edit=(FloatingActionButton)findViewById(R.id.floatingActionButton);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog alert11 = builder1.create();
                //Intent t=getIntent();
                HashMap<String, String> posteditParams;
                posteditParams = new HashMap<String, String>();
                //t.getStringExtra(nativeuid);
                HashMap<String, String> user = session.getUserDetails();

                // name
                String id = user.get(SessionManager.KEY_ID);

                // email
                String email = user.get(SessionManager.KEY_EMAIL);


                Toast.makeText(HomeActivity.this,id,Toast.LENGTH_LONG).show();
                posteditParams.put("id",id);
                invokeEditWS(posteditParams);




                builder1.setMessage("Write your message here.");
                builder1.setCancelable(true);
                builder1.setView(textEntryView);


                builder1.setPositiveButton(
                        "Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //dialog.cancel();
                            }
                        });

                //alert11.show();

                alert11.show();


            }
        });



        /**
         * Call this function whenever you want to check user login
         * This will redirect user to LoginActivity is he is not
         * logged in
         * */

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // name
        String id = user.get(SessionManager.KEY_ID);

        // email
        String email = user.get(SessionManager.KEY_EMAIL);

        // displaying user data
        //lblName.setText(Html.fromHtml("UserName: <b>" + name + "</b>"));
        //lblEmail.setText(Html.fromHtml("Password: <b>" + email + "</b>"));


        /**
         * Logout button click event
         * */

    }

    public void invokeEditWS(HashMap<String, String> params) {
        // Show Progress Dialog
        //prgDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                USER_EDITPROFILE_URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                       // prgDialog.hide();
                        try {
                            Log.i(TAG, "Sign Up Response :: " + response);
                            if(response.getString("status").equalsIgnoreCase("success")){

                                home_name.setText(response.getString("name"));
                                home_dob.setText(response.getString("dateofbirth"));
                                home_mobile.setText(response.getString("mobile"));
                                home_email.setText(response.getString("email"));

                            }
                            else {
                                Toast.makeText(HomeActivity.this,response.getString("status"),Toast.LENGTH_LONG).show();
                            }





                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }



                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //prgDialog.hide();
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //params.put("Authorization", Const.CAD_ACCESS_TOKEN);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG);

    }




}