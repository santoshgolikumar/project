package myapps.com.nativesocial5;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * Register Activity Class
 */
public class RegisterActivity extends Activity {
    // Progress Dialog Object
    ProgressDialog prgDialog;
    // Error Msg TextView Object
    private String TAG = "wb";
    EditText cp;
    TextView rp;

    TextView errorMsg;
    // Name Edit View Object
    EditText nameET;
    // Email Edit View Object
    EditText emailET;
    // Passwprd Edit View Object
    EditText pwdET;
    public static final String USER_SIGNUP_URL="http://35.167.108.80:8080/HappServices/happ/test/signup";
    //private ProgressDialog mProgressDialog = null;
    public static final String OTPKEY="otp";
    public static String EMAILKEY="otp";
    public static String O="o";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        // Find Error Msg Text View control by ID
        errorMsg = (TextView)findViewById(R.id.register_error);
        // Find Name Edit View control by ID
        nameET = (EditText)findViewById(R.id.registerName);
        // Find Email Edit View control by ID
        emailET = (EditText)findViewById(R.id.registerEmail);
        // Find Password Edit View control by ID
        pwdET = (EditText)findViewById(R.id.registerPassword);
        cp=(EditText)findViewById(R.id.registerConPassword);
        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);
    }

    /**
     * Method gets triggered when Register button is clicked
     *
     * @param view
     */
    public void registerUser(View view){
        // Get NAme ET control value
        String name = nameET.getText().toString();
        // Get Email ET control value
        String email = emailET.getText().toString();
        // Get Password ET control value
        String password = pwdET.getText().toString();
        // Instantiate Http Request Param Object
        HashMap<String, String> postDataParams;
        postDataParams = new HashMap<String, String>();



        // When Name Edit View, Email Edit View and Password Edit View have values other than Null
        if(Utility.isNotNull(name) && Utility.isNotNull(email) && Utility.isNotNull(password)){
            // When Email entered is Valid
            //if(!pwdET.getText().toString().equals(cp.getText().toString()))
               // Toast.makeText(RegisterActivity.this,"Passwords Do Not Match",Toast.LENGTH_LONG).show();
            if(Utility.validate(email)&&pwdET.getText().toString().equals(cp.getText().toString())){
                // Put Http parameter name with value of Name Edit View control

                postDataParams.put("name",name);

                // Put Http parameter username with value of Email Edit View control

                postDataParams.put("email_id",email);

                // Put Http parameter password with value of Password Edit View control
                postDataParams.put("password",password);

                // Invoke RESTful Web Service with Http parameters
                invokeWS(postDataParams);
            }
            // When Email is invalid
            else{
                Toast.makeText(getApplicationContext(), "Please enter valid email", Toast.LENGTH_LONG).show();
            }
        }
        // When any of the Edit View control left blank
        else{
            Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     */
    public void invokeWS(HashMap<String, String> params) {
        // Show Progress Dialog
        prgDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                USER_SIGNUP_URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        prgDialog.hide();
                        try {
                            Log.i(TAG, "Sign Up Response :: " + response);
                            //Toast.makeText(RegisterActivity.this,response.getString("status"),Toast.LENGTH_LONG).show();
                            //Toast.makeText(RegisterActivity.this,response.getString("emp_id"),Toast.LENGTH_LONG).show();
                            if(response.getString("status").equalsIgnoreCase("successfully registered")){
                                EMAILKEY=response.getString("Email_id");
                                O=response.getString("emp_id");
                            Intent x=new Intent(RegisterActivity.this,OTPActivity.class);
                                //Intent email=new Intent(RegisterActivity.this,EmailActivity.class);
                                //email.putExtra(EMAILKEY,response.getString("Email_id"));
                            x.putExtra(OTPKEY,response.getString("emp_id"));

                            startActivity(x);}




                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }



                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                prgDialog.hide();
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //params.put("Authorization", Const.CAD_ACCESS_TOKEN);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG);

    }





    /**
     * Method which navigates from Register Activity to Login Activity
     */
  /*  public void navigatetoLoginActivity(View view){
        Intent loginIntent = new Intent(getApplicationContext(),MainActivity.class);
        // Clears History of Activity
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
    }

    /**
     * Set degault values for Edit View controls
     */


}